import scrapy
from scrapy.item import Item, Field
from scrapy.http import Request, FormRequest


class DetailsSearch(scrapy.Spider):

    name = 'details_search'
    allowed_domains = 'https://www.reformagkh.ru/'
    start_urls = ["https://www.reformagkh.ru/search/houses-advanced"]

    # def parse(self, response):
    #     SET_SELECTOR = '//table[@class="table al-edges al-center"]'
    #     for i in response.xpath(SET_SELECTOR):
    #         yield {
    #             'link': i.xpath('.//tr/td/a')#.extract_first(),
    #         }
    
    def parse(self, response):
        return FormRequest.from_response(response,
                                    formxpath="//form[@id='house-advanced']",
                                    formdata={'region': 'Красноярский (край)',
                                              'region-guid': 'db9c4f8b-b706-40e2-b2b4-d31b98dcd3d1',
                                              'district': '',
                                              'district-guid': '',
                                              'settlement':'Красноярск (г)',
                                              'settlement-guid': '9b968c73-f4d4-4012-8da8-3dacd4d4c1bd',
                                              'street': '',
                                              'street-guid':'',
                                              'house':'',
                                              'house-guid':''}
                                    ).text
    